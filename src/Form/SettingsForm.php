<?php

namespace Drupal\fields_from_json\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide settings for Fields From JSON module.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The existing entity type IDs on this site.
   *
   * @var \Drupal\Core\Entity\EntityTypeInferca[]
   */
  protected $entityTypes = [];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);

    $instance->entityTypes = $container->get('entity_type.manager')->getDefinitions();

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fields_from_json_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    $config_names = [];

    $config_names[] = 'fields_from_json.settings';

    foreach ($this->entityTypes as $entity_type) {
      if (!$entity_type->entityClassImplements(FieldableEntityInterface::class)) {
        continue;
      }
      $config_names[] = 'fields_from_json.' . $entity_type->id() . '.settings';
    }

    return $config_names;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $field_type = NULL) {
    $description = $this->t('Copy/Paste your field definitions as JSON content in the boxes below.') . ' ';
    $description .= Link::fromTextAndUrl($this->t('See an example here'), Url::fromUri(
      file_create_url(drupal_get_path('module', 'fields_from_json_demo') . '/data/node_fields.json'),
      ['attributes' => ['target' => '_blank']]
    ))->toString();

    $form['container'] = [
      '#type' => 'item',
      '#title' => $this->t('Field definitions'),
      '#markup' => $description,
    ];

    $form['container'] = [
      '#type' => 'item',
      '#title' => $this->t('Field definitions'),
      '#markup' => $description,
    ];

    $entity_type_ids = [];
    foreach ($this->entityTypes as $entity_type) {
      if (!$entity_type->entityClassImplements(FieldableEntityInterface::class)) {
        continue;
      }

      $entity_type_id = $entity_type->id();

      $entity_type_ids[] = $entity_type_id;

      $config = $this->config('fields_from_json.' . $entity_type_id . '.settings');

      $form['container'][$entity_type_id] = [
        '#type' => 'details',
        '#title' => $entity_type->get('label'),
        '#open' => FALSE,
      ];

      foreach ($config->getRawData() ?? [] as $field_name => $field_info) {
        // Field info.
        $form['container'][$entity_type_id][$field_name] = [
          '#type' => 'details',
          '#title' => $field_info['label'] ?? $field_name,
          '#open' => FALSE,
        ];

        // Check field info by bundle.
        foreach (array_keys($field_info['bundles'] ?? []) as $bundle_id) {
          $field = FieldConfig::load($entity_type_id . '.' . $bundle_id . '.' . $field_name);

          $field_status = $field instanceof FieldConfig ? 'status' : 'warning';

          $bundle_field_info = !$field instanceof FieldConfig ? [] : [
            'type' => $field->getType(),
            'name' => $field->getName(),
            'label' => $field->getLabel(),
          ];

          $form['container'][$entity_type_id][$field_name][$bundle_id] = [
            '#type' => 'item',
            '#title' => $bundle_id . ' : ' . implode(' | ', array_filter($bundle_field_info, function ($value) {
              return \is_string($value);
            })),
            '#wrapper_attributes' => ['class' => ['messages', 'messages--' . $field_status]],
          ];
        }
      }

      $form['container'][$entity_type_id][$entity_type_id . '_content'] = [
        '#type' => 'textarea',
        '#placeholder' => $this->t('Copy/Paste JSON here'),
        '#default_value' => Json::encode($config->getRawData()),
      ];
    }

    $form_state->set('entity_type_ids', $entity_type_ids);

    // Legend
    $legend_ok = '<div class="messages messages--status">' . $this->t('This box describes a field that was created correctly.') . '</div>';
    $legend_warning = '<div class="messages messages--warning">' . $this->t('This box describes a field that was not created yet.') . '</div>';
    $legend_ko = '<div class="messages messages--error">' . $this->t('This box describes a field that is not possible to create.') . '</div>';

    $form['container']['legend'] = [
      '#type' => 'item',
      '#title' => $this->t('Legend'),
      '#markup' => $legend_ok . ' ' . $legend_warning . ' ' . $legend_ko,
      '#attributes' => ['id' => ['field-status-legend']],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->get('entity_type_ids') ?? [] as $entity_type_id) {
      if (!empty($json = $form_state->getValue($entity_type_id . '_content'))) {
        if (!empty($decoded = Json::decode($json)) && !$decoded) {
          $form_state->setErrorByName(
            $entity_type_id . '_content',
            $this->t('Could not decode JSON properly for @entity_type_id.', [
              '@entity_type_id' => $entity_type_id,
            ])
          );
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->get('entity_type_ids') ?? [] as $entity_type_id) {
      $config = $this->config('fields_from_json.' . $entity_type_id . '.settings');
      $json = Json::decode($form_state->getValue($entity_type_id . '_content'));
      foreach ($json as $key => $value) {
        $config->set($key, $value);
      }
      $config->save();
    }

    $this->messenger()->addMessage($this->t('Settings have been saved.'));
  }

}
