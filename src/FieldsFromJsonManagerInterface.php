<?php

namespace Drupal\fields_from_json;

/**
 * Service to automatically create fields.
 */
interface FieldsFromJsonManagerInterface {

  /**
   * Retrieve schemas data.
   *
   * @return array
   *   The list of fields to be created, keyed by the entity type ID.
   *
   * @code
   * $definitions = [
   *   'node' => [
   *     'testman' => [
   *       // Base info.
   *       'name'    => 'myfield',
   *       'label'   => 'This is my field',
   *       'type'    => 'string',
   *       // Force reset if field exists.
   *       'force'   => TRUE,
   *       // Field info by bundles.
   *       'bundles' => [
   *         'page' => [
   *           'label'    => 'This field is wonderful',
   *           // Dipslay settings.
   *           'displays' => [
   *             'form' => [
   *               'default' => [
   *                 // Force display on edit form.
   *                 // 'region' => 'hidden' by default.
   *                 'region' => 'content',
   *               ],
   *             ],
   *             'view' => [
   *               'default' => [
   *                  'label' => 'above',
   *               ],
   *               'teaser'  => [
   *                  'label' => 'hidden',
   *               ],
   *             ],
   *           ],
   *         ],
   *       ],
   *     ],
   *   ],
   * ];
   * @endcode
   */
  public function getFieldsDefinitions();

  /**
   * Provides field storage definitions for a content entity type.
   *
   * @param string $entity_type_id
   *   A given entity type ID.
   * @param array $list
   *   A given list of field definitions.
   *
   * @return \Drupal\Core\Field\FieldStorageDefinitionInterface[]
   *   An array of field storage definitions, keyed by field name.
   */
  public function createEntityFieldStorages(string $entity_type_id, array $list = []);

  /**
   * Create fields on entity types on-the-fly.
   *
   * @param string $entity_type_id
   *   A given entity type ID.
   * @param array $list
   *   A given list of field definitions.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   An array of bundle field definitions, keyed by bundle and field_name.
   */
  public function createEntityFields(string $entity_type_id, array $list = []);

}
