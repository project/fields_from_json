<?php

/**
 * @file
 * Contains hook examples for this module.
 */

/**
 * Implements hook_fields_from_json_definitions().
 */
function hook_fields_from_json_definitions(array &$definitions) {
  // List of fields from a JSON file.
  $node_fields = json_decode(
    file_get_contents(
      drupal_get_path('module', 'fields_from_json_demo') . '/data/node_fields.json'
    )
  );

  $definitions['node'] = array_merge($definitions['node'] ?? [], $node_fields);

  // Manually defined fields.
  $definitions['node'] = [
    'demo_field' => [
      'name'    => 'demo_field',
      'label'   => 'This is my field',
      'type'    => 'string',
      // Create this field once and only.
      'force'   => FALSE,
      'bundles' => [
        'page' => [
          'label'    => 'Oulala',
          'displays' => [
            'form' => [
              'default' => [
                // Force display on edit form.
                'region' => 'content',
              ],
            ],
            'view' => [
              'default' => [
                'label' => 'above',
              ],
              'teaser'  => [
                'label' => 'hidden',
              ],
            ],
          ],
        ],
      ],
    ],
  ];
}
