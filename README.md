CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

The Fields From Json module saves you time by creating fields from a JSON file 
which is particularly helpful during the first development phase of a website
when one needs to create all custom fields manually through the Field UI. 

This module is meant for development purpose and should not be installed on a 
server that faces the internet.

* For a full description of the module visit
https://www.drupal.org/project/fields_from_json

* To submit bug reports and feature suggestions, or to track changes visit
https://www.drupal.org/project/issues/fields_from_json


REQUIREMENTS
------------

This module does not require any additional modules outside of Drupal core.


INSTALLATION
------------

Install the Fields From Json module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for more information.


CONFIGURATION
-------------

1. Enable Fields File Proxy, either via "Extend" (/admin/modules) or via drush:
$ drush en --yes fields_from_json

2. Option 1: upload the Json file from the UI, at Configuration > Fields From Json 
Settings (admin/config/system/fields_from_json)

3. Option 2: use the <code>hook_fields_from_json_definitions()</code>


MAINTAINERS
-----------

* Matthieu Scarset (matthieuscarset) - https://www.drupal.org/u/matthieuscarset
